# Hello, we are heading to the moon. 🚀

'''
happy
way
home
'''

import turtle

def reset(t):
	t.home()
	t.clear()

def siral(t,y,r):
	for x in range(0,y):
		t.left(1)
		t.forward(1+r*(x/y))

def main():
	t=turtle.Turtle()
	reset(t)
	siral(t,560,.55)

if __name__ == '__main__':
	print ('MY NAME IS TURTLE!!')
	main()